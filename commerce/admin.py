# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django.db.models import Max
from django.template.context import Context
from django.template.loader import get_template
from django.utils.translation import ugettext_lazy as _
from parler.admin import TranslatableAdmin
from cms.admin.placeholderadmin import PlaceholderAdminMixin, FrontendEditableAdminMixin
from shop.admin.defaults import customer
from shop.admin.defaults.order import OrderAdmin
from shop.models.defaults.order import Order
from shop.admin.order import PrintInvoiceAdminMixin
from shop.admin.delivery import DeliveryOrderAdminMixin
from adminsortable2.admin import SortableAdminMixin, PolymorphicSortableAdminMixin
from shop.admin.product import CMSPageAsCategoryMixin, ProductImageInline, InvalidateProductCacheMixin, CMSPageFilter
from polymorphic.admin import (PolymorphicParentModelAdmin, PolymorphicChildModelAdmin,
                               PolymorphicChildModelFilter)
from commerce.models import Product, Commodity, WearableModelVariant, WearableModel
from commerce.models import Brand, Category


admin.site.site_header = "Web.mvc Pro Administration"


@admin.register(Order)
class OrderAdmin(PrintInvoiceAdminMixin, DeliveryOrderAdminMixin, OrderAdmin):
    pass


admin.site.register(Brand, admin.ModelAdmin)
admin.site.register(Category, admin.ModelAdmin)

__all__ = ['customer']


@admin.register(Commodity)
class CommodityAdmin(InvalidateProductCacheMixin, SortableAdminMixin, TranslatableAdmin, FrontendEditableAdminMixin,
                     PlaceholderAdminMixin, CMSPageAsCategoryMixin, PolymorphicChildModelAdmin):
    """
    Since our Commodity model inherits from polymorphic Product, we have to redefine its admin class.
    """
    base_model = Product
    fields = ['product_name', 'slug', 'product_code',
              'unit_price', 'active', 'brand']
    filter_horizontal = ['cms_pages']
    inlines = [ProductImageInline]
    prepopulated_fields = {'slug': ['product_name']}


class WearableModelInline(admin.TabularInline):
    model = WearableModelVariant
    extra = 0

@admin.register(WearableModel)
class WearableModelAdmin(InvalidateProductCacheMixin, SortableAdminMixin, TranslatableAdmin, FrontendEditableAdminMixin,
                      CMSPageAsCategoryMixin, PlaceholderAdminMixin, PolymorphicChildModelAdmin):
    base_model = Product
    fieldsets = [
        (None, {
            'fields': ['product_name', 'slug', 'active'],
        }),
        (_("Translatable Fields"), {
            'fields': ['description', 'short_description'],
        }),
        (_("Properties"), {
            'fields': ['brand', 'weight', 'color', 'categories'],
        }),
    ]
    filter_horizontal = ['cms_pages']
    inlines = [ProductImageInline, WearableModelInline]
    prepopulated_fields = {'slug': ['product_name']}

    def save_model(self, request, obj, form, change):
        if not change:
            # since SortableAdminMixin is missing on this class, ordering has to be computed by hand
            max_order = self.base_model.objects.aggregate(
                max_order=Max('order'))['max_order']
            obj.order = max_order + 1 if max_order else 1
        super(WearableModelAdmin, self).save_model(request, obj, form, change)

    def render_text_index(self, instance):
        template = get_template('search/indexes/commerce/commodity_text.txt')
        return template.render(Context({'object': instance}))
    render_text_index.short_description = _("Text Index")


@admin.register(Product)
class ProductAdmin(PolymorphicSortableAdminMixin, PolymorphicParentModelAdmin):
    base_model = Product
    child_models = [WearableModel, Commodity]
    list_display = ['product_name', 'get_price', 'product_type', 'active']
    list_display_links = ['product_name']
    search_fields = ['product_name']
    list_filter = [PolymorphicChildModelFilter, CMSPageFilter]
    list_per_page = 250
    list_max_show_all = 1000

    def get_price(self, obj):
        return str(obj.get_real_instance().get_price(None))

    get_price.short_description = _("Price starting at")
