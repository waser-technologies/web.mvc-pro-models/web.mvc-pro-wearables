from decimal import Decimal
from django.utils.translation import ugettext_lazy as _
import os.path

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

DSS = {
    'settings': {
        'SHOP_EDITCART_NG_MODEL_OPTIONS': "{updateOn: 'default blur', debounce: {'default': 2500, 'blur': 0}}",
    }
}
